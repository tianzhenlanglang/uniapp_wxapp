import { defineStore } from 'pinia'
import { ref } from 'vue'
export const usePageStateStore = defineStore('pagestate', () => {
  const detialstate = ref(false)
  return { detialstate }
})
