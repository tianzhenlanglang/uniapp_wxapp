import { defineStore } from 'pinia'
import { ref } from 'vue'
export const useUserStore = defineStore(
  'users',
  () => {
    /**
     *手机号码
     **/
    const phonenumber = ref('')
    /**
     *租户ID
     **/
    const tenantId = ref('')
    /**
     *在线状态
     **/
    const onlinestate = ref(false)
    /**
     *用户名称
     **/
    const username = ref<string | undefined>('')
    /**
     *微信openid
     **/
    const wxopenid = ref('')

    const phoneid = ref<string>('')
    // const doubleCount = computed(() => count.value * 2)
    // function increment() {
    // 	count.value++
    // }
    return { phonenumber, tenantId, onlinestate, username, wxopenid, phoneid }
  },
  {
    persist: {
      storage: {
        getItem(key) {
          return uni.getStorageSync(key)
        },
        setItem(key, value) {
          uni.setStorageSync(key, value)
        }
      }
    }
  }
)
