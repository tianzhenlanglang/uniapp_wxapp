import type { consumablesDto } from '../consumables/types'

export interface workOrderDto {
  orderNumber: string
  roomOrAreaCode: string
  roomAreaId: string
  tasktypeId: string
  acceptDuration: number
  completeDuration: number
  content: string
  addTime: string
  acceptTime?: string
  completeTime?: string
  transferedPhoneId?: string
  transferedPhoneName?: string
  transferedTime?: string
  orderStatu: OrderStatu
  resumedTime?: string
  cancellationReason?: string
  pauseReason?: string
  enmergency: boolean
  //currentAlertLevel: number
  acceptedAlertLevel: number
  completedAlertLevel: number
  acceptedUserId?: string
  acceptedUserName?: string
  createId: string
  createUserName: string
  notification: boolean
  id: string
  actionsNumbers?: Array<ActionsNumberDto>
  currentPhoneNumber?: string
  currentPhoneNumberId?: string
  canAction: boolean
}

export interface ActionsNumberDto {
  phoneNumber: string
  name: string
  workOrderId: string
  showLable: string
  id: string
}
interface PageQuery {
  /** 查询条件 */
  filter?: string
  /** 排序字段 */
  sorting?: string
  /** 跳过数量 */
  skipcount: number
  /** 每页数量 */
  maxresultcount: number
}

export interface orderQuery extends PageQuery {
  PhoneId: string
  queryParam: Queryparam
}

export enum Queryparam {
  /// <summary>
  /// 代办
  /// </summary>
  Index,
  /// <summary>
  /// 全部
  /// </summary>
  All,
  /// <summary>
  /// 未接单
  /// </summary>
  UnAccept,
  /// <summary>
  /// 已接单
  /// </summary>
  Accept,
  /// <summary>
  /// 已完成
  /// </summary>
  Completed,
  /// <summary>
  /// 通知
  /// </summary>
  Notify
}

/**
 * 分页查询参数
 */
export interface PageSize {
  pageNum: number
  pageSize: number
}

/**
 * 分页响应对象
 */
interface PageResult<T> {
  /** 数据列表 */
  items: T
  /** 总数 */
  totalCount: number
}

export type OrderPageResult = PageResult<workOrderDto[]>

export enum OrderStatu {
  /// <summary>
  /// 创建
  /// </summary>
  Created,
  /// <summary>
  /// 接受
  /// </summary>
  Accepted,
  /// <summary>
  /// 完成
  /// </summary>
  Completed,
  /// <summary>
  /// 转单
  /// </summary>
  Transfered,
  /// <summary>
  /// 取消
  /// </summary>
  Canceled,
  /// <summary>
  /// 暂停
  /// </summary>
  Paused,
  /// <summary>
  /// 恢复
  /// </summary>
  Resumed
}

export interface CreateWorkOrderDto {
  phonenumber: string
  phoneid: string
  ceateUserName?: string
  roomOrAreaCode: string
  tasktypeId: string
  content?: string
  enmergency: boolean
}
export interface CreateReultDto {
  code: CodeEnum
}
export enum CodeEnum {
  NoRoomArea,
  NoTaskType,
  Success,
  NoGroups,
  /// <summary>
  /// 没有对应的区域职责
  /// </summary>
  NoAllocation,
  /// <summary>
  /// 无人打卡上班
  /// </summary>
  NoLogin,

  Error
}

export interface CanceledOrPausedDto {
  reason: string
  createId: string
}

export interface TransferDto {
  actionId: string
  transferedId: string
}

export interface OrderConsumabsDto {
  consumabDto: consumablesDto
  quantity: number
  id: string
}

export interface AddConsumabDto {
  id?: string
  workOrderID: string
  consunabID: string
  quantity: number
}
export interface IndexTotalDto {
  total: number
  unCompleteCount: number
  completeCount: number
}
