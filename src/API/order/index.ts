import type {
  CreateWorkOrderDto,
  CreateReultDto,
  TransferDto,
  OrderConsumabsDto,
  AddConsumabDto
} from './types'
import type {
  OrderPageResult,
  orderQuery,
  workOrderDto,
  CanceledOrPausedDto,
  IndexTotalDto
} from './types'
import { http } from '@/utils/http'
// export async function GetTestOrder(phone: string): Promise<workOrderDto> {
//   return await http<workOrderDto>({
//     url: '/api/Orders/GetWxOrder',
//     method: 'GET',
//     data: {
//       phone
//     }
//   })
// }
export async function GetOrder(data: orderQuery): Promise<OrderPageResult> {
  return await http<OrderPageResult>({
    url: '/api/app/work-order/wx-work-order-list',
    method: 'GET',
    data
  })
}
export async function GetOrderByID(
  id: string,
  phoneId: string
): Promise<workOrderDto> {
  return await http<workOrderDto>({
    url: '/api/WxApp/GetWxWorkOrder',
    method: 'GET',
    data: {
      id,
      phoneId
    }
  })
}

export async function CreateOrder(
  data: CreateWorkOrderDto
): Promise<CreateReultDto> {
  return await http<CreateReultDto>({
    url: '/api/WxApp/WxCreateOrder',
    method: 'POST',
    data
  })
}

export async function AcceptOrder(
  id: string,
  phoneId: string
): Promise<workOrderDto> {
  return await http<workOrderDto>({
    url: `/api/WxApp/AcceptOrder?id=${id}&phoneId=${phoneId}`,
    method: 'POST'
  })
}

export async function CompleteOrder(
  id: string,
  phoneId: string
): Promise<workOrderDto> {
  return await http<workOrderDto>({
    url: `/api/WxApp/CompleteOrder?id=${id}&phoneId=${phoneId}`,
    method: 'POST'
  })
}

export async function GetServerRuntime(): Promise<string> {
  return await http<string>({
    url: '/api/WxApp/GetServerRuntime',
    method: 'GET'
  })
}

export async function CanceledOrder(
  id: string,
  data: CanceledOrPausedDto
): Promise<workOrderDto> {
  return await http<workOrderDto>({
    url: `/api/WxApp/CancelOrder?id=${id}`,
    method: 'POST',
    data
  })
}
export async function PausedOrder(
  id: string,
  data: CanceledOrPausedDto
): Promise<workOrderDto> {
  return await http<workOrderDto>({
    url: `/api/WxApp/PasuedOrder?id=${id}`,
    method: 'POST',
    data
  })
}
///api/WxApp/ResumedOrder
export async function ResumedOrder(
  id: string,
  phoneId: string
): Promise<workOrderDto> {
  return await http<workOrderDto>({
    url: `/api/WxApp/ResumedOrder?id=${id}&phoneId=${phoneId}`,
    method: 'POST'
  })
}

export async function TransferOrder(
  id: string,
  data: TransferDto
): Promise<workOrderDto> {
  return await http<workOrderDto>({
    url: `/api/WxApp/TransferOrder?id=${id}`,
    method: 'POST',
    data
  })
}

export async function getconsumablesbyid(
  id: string
): Promise<Array<OrderConsumabsDto>> {
  return await http<Array<OrderConsumabsDto>>({
    url: `/api/app/work-order/${id}/consumabs`,
    method: 'GET'
  })
}

export async function addconsumbale(data: AddConsumabDto): Promise<any> {
  data.id = data.id || '00000000-0000-0000-0000-000000000000'
  return await http<any>({
    url: `/api/app/work-order/consumab`,
    method: 'POST',
    data
  })
}

export async function updateconsumables(
  id: string,
  data: AddConsumabDto
): Promise<any> {
  return await http<any>({
    url: `/api/app/work-order/${id}/consumab`,
    method: 'PUT',
    data
  })
}

export async function deleteconsumables(id: string): Promise<any> {
  return await http<any>({
    url: `/api/app/work-order/${id}/consumab`,
    method: 'DELETE'
  })
}

export async function GetIndexTotal(id: string): Promise<IndexTotalDto> {
  return await http<IndexTotalDto>({
    url: `/api/WxApp/GetWxIndexTotal?phoneId=${id}`,
    method: 'GET'
  })
}
