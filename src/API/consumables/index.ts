import { http } from '@/utils/http'
import type { consumablesDto, consumablestypesDto } from './types'
export async function getallconsumablestypes(): Promise<
  Array<consumablestypesDto>
> {
  return await http<Array<consumablestypesDto>>({
    url: '/api/app/consumables-type/list',
    method: 'GET'
  })
}

export async function getconsumablesbytype(
  type?: string
): Promise<Array<consumablesDto>> {
  type = type || '00000000-0000-0000-0000-000000000000'
  return await http<Array<consumablesDto>>({
    url: `/api/app/consumabs/consumables-by-category-id/${type}`,
    method: 'GET'
  })
}
