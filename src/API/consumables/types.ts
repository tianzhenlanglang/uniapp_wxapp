export interface consumablesDto {
  id: string
  consumabName: string
  consumabSpec?: string
  consumabUnit?: string
  consumabRemark?: string
  consumablesTypeId: string
  consumablesType: consumablestypesDto
  showName: string
}

export interface consumablestypesDto {
  id: string
  name: string
  description: string
}
export interface SelectParams {
  value: string
  label: string
  disabled?: boolean
}
