/**
 * 组织机构
 */
export interface organizationsdto {
  // tenantId?: string
  name: string
  description?: string
  parentId?: string | null
  id?: string
}
