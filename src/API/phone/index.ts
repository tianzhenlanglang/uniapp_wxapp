import { http } from '@/utils/http'
import type { PhoneDto } from './types'
export async function GetActivePhonesByPhoneID(
  phoneid: string
): Promise<Array<PhoneDto>> {
  return await http<Array<PhoneDto>>({
    url: `/api/app/phone/active-phones-by-phone-id/${phoneid}`,
    method: 'GET'
  })
}
