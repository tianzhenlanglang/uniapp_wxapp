import type { organizationsdto } from '@/API/organizations/types'
export interface PhoneDto {
  phoneNumber: string
  name: string
  openId: string
  organizationId: string
  organization: organizationsdto
  loginState: boolean
  lable: string
  id: string
}
