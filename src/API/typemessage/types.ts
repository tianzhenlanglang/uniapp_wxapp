import type { organizationsdto } from '@/API/organizations/types'
export interface TaskTypeDto {
  id: string
  name: string
  organizationId: string
  organization: organizationsdto
  description?: string
  acceptTime: number
  completeTime: number
  notification: boolean
}
export interface SelectParams {
  value: string
  label: string
  disabled?: boolean
}

export interface TypeMessageDto {
  id: string
  title: string
  message?: string
  tasktypeId: string
  pinyin?: string
  renPing?: string
}
