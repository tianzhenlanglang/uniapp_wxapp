import { http } from '@/utils/http'
import type { TaskTypeDto, TypeMessageDto } from './types'

export async function getalltasktypelist(): Promise<Array<TaskTypeDto>> {
  return await http<Array<TaskTypeDto>>({
    url: '/api/app/tasktype/list',
    method: 'GET'
  })
}

export async function getmessagelistbytypeid(
  id: string
): Promise<Array<TypeMessageDto>> {
  return await http<Array<TypeMessageDto>>({
    url: `/api/app/types-message/message-by-type-id/${id}`,
    method: 'GET'
  })
}
