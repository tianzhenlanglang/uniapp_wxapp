export interface TenantDto {
  isAvailable: boolean
  id?: string
  name?: string
}

export interface PhoneSetDto {
  phoneNumber: string
  openId: string
}

export interface WxSetPhoneRetrunDto {
  code: string
  phoneName?: string
  phoneId: string
}

export interface AllocationAreaActiveReturnDto {
  code: string
  message: string
}
