import { http } from '@/utils/http'
import type {
  TenantDto,
  PhoneSetDto,
  WxSetPhoneRetrunDto,
  AllocationAreaActiveReturnDto
} from './types'

export async function GetTenant(): Promise<TenantDto> {
  return await http<TenantDto>({
    url: '/api/WxApp/GetCurrentTenant',
    method: 'GET'
  })
}
export async function SetPhones(
  data: PhoneSetDto
): Promise<WxSetPhoneRetrunDto> {
  return await http<WxSetPhoneRetrunDto>({
    url: '/api/WxApp/WxSetPhone',
    method: 'POST',
    data
  })
}

export async function ActivePhonesState(
  phineid: string,
  active: boolean
): Promise<AllocationAreaActiveReturnDto> {
  return await http<AllocationAreaActiveReturnDto>({
    url: `/api/WxApp/ChangeActive?phoneid=${phineid}&active=${active}`,
    method: 'POST'
  })
}
