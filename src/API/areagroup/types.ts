import type { organizationsdto } from '@/API/organizations/types'

export interface areagroupdto {
  name: string
  areaCode?: string
  organizationId: string
  description?: string
  organization: organizationsdto
  //   roomAreas?: Array<roomdto>
  id: string
}

export interface RetrunDto {
  code: string
  message: string
}

export interface allocationAreaDto {
  name?: string
  phoneId: string
  id: string
}

export interface PhoneDto {
  phoneNumber: string
  name: string
  openId: string
  organizationId: string
  loginState: boolean
  lable: string
  id: string
}

export interface OnlineStateDto {
  areaGroupDto: areagroupdto
  allocationAreaDtos: Array<allocationAreaDto>
  phoneDtos: Array<PhoneDto>
  activeMemberCount: number
  allMemberCount: number
}
