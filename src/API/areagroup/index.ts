import type { areagroupdto, RetrunDto, OnlineStateDto } from './types'
import { http } from '@/utils/http'

export async function GetAreaByPhoneID(
  phoneid: string
): Promise<Array<areagroupdto>> {
  return await http<Array<areagroupdto>>({
    url: '/api/WxApp/GetAreaGroupsByPhoneID',
    method: 'GET',
    data: {
      phoneid
    }
  })
}

export async function GetOnlineStateByPhoneID(
  phoneid: string
): Promise<Array<OnlineStateDto>> {
  return await http<Array<OnlineStateDto>>({
    url: '/api/WxApp/GetOnlineStateByPhoneId',
    method: 'GET',
    data: {
      phoneid
    }
  })
}

export async function SetAreagroupByPhoneId(
  id: string,
  data: Array<areagroupdto>
): Promise<RetrunDto> {
  return await http<RetrunDto>({
    url: `/api/WxApp/SetAreagroupByPhoneId?phoneid=${id}`,
    method: 'POST',
    data
  })
}

export async function GetActiveAreaByPhoneID(
  id: string
): Promise<Array<areagroupdto>> {
  return await http<Array<areagroupdto>>({
    url: '/api/WxApp/GetActiveAreaGroupByPhoneId',
    method: 'GET',
    data: {
      id
    }
  })
}
